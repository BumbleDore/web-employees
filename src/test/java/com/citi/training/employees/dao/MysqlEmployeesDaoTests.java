package com.citi.training.employees.dao;



import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.model.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MysqlEmployeesDaoTests {

	@Autowired
	MysqlEmployeeDao mysqlEmployeeDao;
	
	
	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlEmployeeDao.create(new Employee(1, "Caolan", 2000));
		
		assertEquals(mysqlEmployeeDao.findAll().size(), 1);
	}
	
	@Test
	@Transactional
	public void test_createAndFindById() {
		mysqlEmployeeDao.create(new Employee(1, "Caolan", 2000));
		Employee test = mysqlEmployeeDao.create(new Employee(2, "olan", 2000));
		
		assertThat(test).isEqualToComparingFieldByField(mysqlEmployeeDao.findById(test.getId()));
		
	}
	
	@Test 
	@Transactional
	public void test_DeleteEmployee() {
		Employee test1 = mysqlEmployeeDao.create(new Employee( "Caolan", 2000));
		Employee delete = mysqlEmployeeDao.create(new Employee( "olan", 2000));
		
		mysqlEmployeeDao.deleteById(delete.getId());
		assertEquals(mysqlEmployeeDao.findAll().size(), 1);
		assertThat(test1).isEqualToComparingFieldByField(mysqlEmployeeDao.findById(test1.getId()));
	
	}
	

}
