package com.citi.training.employees.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import com.citi.training.employees.model.Employee;

public class EmployeeTests {

	private int testId = 53;
    private String testName = "Bob";
    private double testSalary = 25000.00;
    
    @Test
    public void test_Employee_constructor() {
        Employee testEmployee = new Employee(testId, testName, testSalary);;

        assertEquals(testId, testEmployee.getId());
        assertEquals(testName, testEmployee.getName());
        assertEquals(testSalary, testEmployee.getSalary(), 0.0001);
    }

    @Test
    public void test_Employee_toString() {
        String testString = new Employee(testId, testName, testSalary).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testName));
        assertTrue(testString.contains(String.valueOf(testSalary)));
    }
}
